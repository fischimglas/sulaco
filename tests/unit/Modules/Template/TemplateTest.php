<?php

namespace unit\Modules\Template;

use PHPUnit\Framework\TestCase;
use Sulaco\Template\Template;

class TemplateTest extends TestCase
{
    public function testBasics()
    {
        $record = new Template();
        $record->setData([
            'test1' => 'content of variable 1',
        ]);
        $record->setTemplate('Some content "{{test1}}"');

        self::assertEquals('Some content "content of variable 1"', $record->compile());
    }

}
