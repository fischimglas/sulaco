<?php

namespace unit\Modules\Portr;

use PHPUnit\Framework\TestCase;
use PTag\HtmlFactory;
use Sulaco\Portr\TemplateDocument;

class TemplateDocumentTest extends TestCase
{

    public function testBasics()
    {
        $record = new TemplateDocument();
        $record->setData([
            'test1' => 'content of variable 1',
        ]);

        $content = 'Some content "{{test1}}"';
        $record->setContentRichText(HtmlFactory::div(null, $content));
        $record->setContentPlainText($content);
        $record->setSubject($content);
        $record->setTitle($content);

        self::assertEquals('Some content "content of variable 1"', $record->getSubject());
        self::assertEquals('Some content "content of variable 1"', $record->getTitle());
        self::assertEquals('<div>Some content "content of variable 1"</div>', $record->getContentRichText());
        self::assertEquals('Some content "content of variable 1"', $record->getContentPlainText());
    }

}
