<?php

namespace test;

use PHPUnit\Framework\TestCase;
use Sulaco\Fs\File;

class FileTest extends TestCase
{

    public function testGetAbsolutPath()
    {
        $f = new File();
        $res = $f->getAbsolutPath();

        self::assertEquals('', $res);
    }

}
