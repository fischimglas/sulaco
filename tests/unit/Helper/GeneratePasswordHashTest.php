<?php

namespace test;

use PHPUnit\Framework\TestCase;
use Sulaco\Helper\StringHelper;

class GeneratePasswordHashTest extends TestCase
{

    /**
     * @dataProvider dataProvider
     */
    public function testGeneratePasswordHash($len, $expected)
    {
        $pw = StringHelper::generatePasswordHash($len);
        self::assertIsString($pw);
        self::assertEquals($expected, strlen($pw));
    }

    public function dataProvider(): array
    {
        return [
            [0, 0],
            [1, 1],
            [100, 100],
            [121111, 121111]
        ];
    }
}
