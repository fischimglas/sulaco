<?php
declare(strict_types=1);

namespace Sulaco\Contract;

use PTag\SerializeableInterface;

interface Serializeable extends SerializeableInterface
{
}
