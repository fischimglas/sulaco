<?php

declare(strict_types=1);

namespace Sulaco\Contract;

interface AddressInterface
{
    public function street(): ?string;

    public function zip(): ?string;

    public function city(): ?string;

    public function district(): ?string;

    public function country(): ?string;
}
