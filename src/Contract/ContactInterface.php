<?php

declare(strict_types=1);

namespace Sulaco\Contract;

interface ContactInterface
{

  public function getName(): string;

  public function getType(): string;

  public function getCoordinates(): array;
}
