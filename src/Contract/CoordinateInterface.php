<?php

declare(strict_types=1);

namespace Sulaco\Contract;

interface CoordinateInterface
{

  public function name(): string;

  public function email(): string;

  public function mobile(): string;

  public function address(): AddressInterface;

}
