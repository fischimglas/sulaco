<?php

declare(strict_types=1);

namespace Sulaco\Contract;

use Sulaco\Portr\Channel\ProcessResult;
use Sulaco\Portr\TemplateDocument;

interface ChannelInterface
{
    /**
     * @param TemplateDocument $templateDocument
     * @return mixed
     */
    public function processTemplate(TemplateDocument $templateDocument): ProcessResult;

    /**
     * Prepare Style with template for this channel in particular
     * @return string
     */
    public function prepareStyle(): string;

    /**
     * Prepare Attachments with template for this channel in particular
     * @return array
     */
    public function prepareAttachments(): array;

    /**
     * Prepare Content with template for this channel in particular
     * @return string
     */
    public function prepareContentRichText(): string;

}
