<?php

declare(strict_types=1);

namespace Sulaco\Contract;

interface FileInterface
{
    public function getAbsolutPath(): string;

    public function getRelativePath(): string;
}
