<?php
declare(strict_types=1);

namespace Sulaco\Template;


interface TemplateInterface
{
    public function setData(array $data): void;

    public function compile(): string;

    public function setTemplate(string $template): void;
}
