<?php

namespace Sulaco\Core\Http\Response;

interface ResponseInterface
{
    public function send();
}
