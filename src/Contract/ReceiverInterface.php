<?php

declare(strict_types=1);

namespace Sulaco\Contract;

interface ReceiverInterface
{
  public function getRole(): string;

  public function getCoordinate(): CoordinateInterface;
}
