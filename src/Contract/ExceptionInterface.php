<?php
declare(strict_types=1);

namespace Sulaco\Contract;


use Sulaco\Model\ExceptionSeverity;

interface ExceptionInterface
{
    public function getSeverity(): string;
}