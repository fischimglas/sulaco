<?php
declare(strict_types=1);

namespace Sulaco\Fs;

use Sulaco\Exception\FileSystemException;

class File
{
    public static ?string $basePath = null;

    private ?string $filePath = null;

    /**
     * @param string|null $path
     */
    public function __construct(?string $path = null)
    {
        $this->setPath($path);
    }

    /**
     * @param string $basePath
     * @param int|null $permissions
     * @return void
     */
    public static function setBasePath(string $basePath, ?int $permissions = 0755): void
    {
        if (!is_dir(dirname($basePath))) {
            mkdir($basePath, $permissions, true);
        }
        self::$basePath = $basePath;
    }

    /**
     * @return string|null
     */
    public static function getBasePath(): ?string
    {
        return self::$basePath;
    }

    /**
     * @param string $fileName
     * @return File
     * @throws FileSystemException
     */
    public static function create(string $fileName): File
    {
        if ($fileName[0] !== '/') {
            $path = self::$basePath . '/' . $fileName;
        } else {
            $path = $fileName;
        }
        if (!is_writeable(dirname($path))) {
            throw new FileSystemException(sprintf('Dir %s is not writeable', dirname($path)));
        }

        touch($path);

        return new File($path);
    }

    /**
     * @param string|null $filePath
     */
    public function setPath(?string $filePath = null): void
    {
        $this->filePath = $filePath;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->filePath;
    }

    /**
     * @return string
     * @throws FileSystemException
     */
    public function read(): string
    {
        if (!$this->getPath() || !$this->fileExists()) {
            throw new FileSystemException(sprintf("File '%s' does not exists", $this->filePath));
        }

        return file_get_contents($this->getAbsolutPath());
    }

    /**
     * @return string
     */
    public function getAbsolutPath(): string
    {
        return (string)(self::$basePath !== null && $this->filePath[0] !== '/' ? sprintf('%s/%s', self::$basePath, $this->filePath) : $this->filePath);
    }

    /**
     * @param mixed $content
     * @param int|null $mode
     * @return bool
     * @throws FileSystemException
     */
    public function write(mixed $content, ?int $mode = 0): bool
    {
        if (!is_writeable(dirname($this->getAbsolutPath()))) {
            throw new FileSystemException(sprintf('Dir %s is not writeable', dirname($this->getAbsolutPath())));
        }

        return (bool)file_put_contents($this->getAbsolutPath(), $content, $mode);
    }

    /**
     * @param string $path
     * @return bool
     */
    public function moveTo(string $path): bool
    {
        $newPath = new File($path);

        $res = rename($this->getAbsolutPath(), $newPath->getAbsolutPath());
        if (!$res) {
            return false;
        }
        $this->filePath = $newPath->getAbsolutPath();

        return true;
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        if (!$this->fileExists()) {
            return false;
        }

        return unlink($this->getAbsolutPath());
    }

    /**
     * @return bool
     */
    public function fileExists(): bool
    {
        return file_exists($this->getAbsolutPath());
    }

    /**
     * @return string
     */
    public function basename(): string
    {
        return basename($this->filePath);
    }

    /**
     * @return string
     */
    public function getContentType(): string
    {
        return mime_content_type($this->getAbsolutPath());
    }
}
