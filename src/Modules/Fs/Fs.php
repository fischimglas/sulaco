<?php
declare(strict_types=1);

namespace Sulaco\Fs;

use Sulaco\Exception\FileSystemException;
use Sulaco\Helper\StringHelper;

class Fs
{
    /**
     * @param string $fileName
     * @return string
     */
    public static function makeFileName(string $fileName): string
    {
        return StringHelper::sanitizeFilename($fileName);
    }

    /**
     * @param string $fileName
     * @param string $data
     * @return File
     * @throws FileSystemException
     */
    public static function writeFile(string $fileName, mixed $data): File
    {
        $file = new File($fileName);
        $file->write($data);

        return $file;
    }

    /**
     * @param string $dir
     * @return bool
     * @throws FileSystemException
     */
    public static function deleteDirectory(string $dir): bool
    {
        array_map(fn($file) => self::deleteFile($file), self::readDirectory($dir));

        return rmdir($dir);
    }

    /**
     * @param string $path
     * @param bool $withAbsolutePath
     * @return array
     * @throws FileSystemException
     */
    public static function readDirectory(string $path, bool $withAbsolutePath = true): array
    {
        if (!is_dir($path)) {
            throw new FileSystemException(sprintf("Given path '%s' is not a directory", $path));
        }

        $dh = opendir($path);
        $files = [];
        while (($f = readdir($dh)) !== false) {
            if ($f !== '..' && $f !== '.') {
                $files[] = $withAbsolutePath ? realpath($path) . '/' . $f : $f;
            }
        }

        return $files;
    }

    /**
     * @param $file
     * @return bool
     */
    public static function deleteFile($file): bool
    {
        $file = new File($file);

        return $file->delete();
    }

    /**
     * @param string $dir
     * @param string|null $action
     * @return void
     * @throws FileSystemException
     */
    public static function includeFiles(string $dir, ?string $action = 'include_once'): void
    {
        foreach (self::readDirectory($dir) as $file) {
            if (is_file($file)) {
                match ($action) {
                    'include_once' => include_once $file,
                    'include' => include $file,
                    'require_once' => require_once $file,
                    'require' => require $file,
                };
            }
        }
    }

    /**
     * @param string $contentType
     * @return string|null
     */
    public static function fileTypeByContentType(string $contentType): ?string
    {
        return match ($contentType) {
            'text/plain' => 'text',
            'image/webp' => 'webp',
            'image/png' => 'png',
            'image/jpg', 'image/jpeg' => 'jpg',
            'image/svg+xml' => 'svg',
            'image/gif' => 'gif',
            'text/html' => 'html',
            'image/x-icon', 'image/vnd.microsoft.icon' => 'ico',
            "application/msword" => "doc",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document" => "docx",
            "application/vnd.ms-excel" => "xls",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" => "xlsx",
            "application/vnd.ms-powerpoint" => "ppt",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation" => "pptx",
            'application/rtf' => 'rtf',
            'application/zip' => 'zip',
            'application/pdf' => 'pdf',
            'application/vnd.rar', 'application/x-rar-compressed' => 'rar',
            'application/x-7z-compressed' => '7z',
            'application/x-tar' => 'tar',
            'application/gzip' => 'gz',
            'text/markdown' => 'md',
            'text/javascript' => 'js',
            'application/json' => 'json',
            'image/bmp' => 'bpm',
            'audio/mpeg' => 'mp3',
            'audio/wav' => 'wav',
            'audio/aac' => 'aac',
            'audio/flac' => 'flac',
            'audio/ogg' => 'ogg',
            'video/mp4' => 'mp4',
            'video/x-msvideo' => 'avi',
            'video/x-matroska' => 'mkv',
            'video/quicktime' => 'mov',
            'video/x-ms-wmv' => 'wmv',
            'application/vnd.android.package-archive' => 'apk',
            'application/vnd.mozilla.xul+xml' => 'xul',
            'application/x-csh' => 'csh',
            'application/x-java-archive' => 'jar',
            'application/x-python-code' => 'pyc',
            'application/x-dosexec' => 'exe',
            'application/x-iso9660-image' => 'iso',
            default => null
        };
    }
}
