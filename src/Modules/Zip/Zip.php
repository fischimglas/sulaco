<?php

declare(strict_types=1);

namespace Sulaco\Zip;

use Sulaco\Exception\FileSystemException;
use Sulaco\Fs\File;
use ZipArchive;

class Zip extends File
{
    private array $files = [];
    private bool $deleteSourcefiles = true;
    private ?string $password = null;

    /**
     * @param array $files
     * @return Zip
     * @throws FileSystemException
     */
    public function addFiles(array $files): self
    {
        array_map(fn($it) => $this->addFile($it), $files);

        return $this;
    }

    /**
     * @param File|string $fileName
     * @return Zip
     * @throws FileSystemException
     */
    public function addFile(File|string $fileName): self
    {
        if (is_string($fileName)) {
            $file = new File($fileName);
            if (!$file->fileExists()) {
                throw new FileSystemException(sprintf('File "%s" does not exist', $file->getAbsolutPath()));
            }
        } else {
            $file = $fileName;
        }
        $this->files[] = $file;

        return $this;
    }

    /**
     * @return $this
     * @throws FileSystemException
     */
    public function compress(): self
    {
        $zip = $this->getZipArchive();

        foreach ($this->files as $file) {
            $targetFilePath = $file->getAbsolutPath();
            if ($file->fileExists()) {
                $zip->addFile($targetFilePath, $file->basename());
            }
        }
        $zip->close();

        if ($this->deleteSourcefiles) {
            foreach ($this->files as $key => $file) {
                $file->delete();
                unset($this->files[$key]);
            }
        }

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param int|null $flags
     * @return ZipArchive
     * @throws FileSystemException
     */
    public function getZipArchive(?int $flags = ZipArchive::CREATE): ZipArchive
    {
        $zipPath = $this->getAbsolutPath();
        $zip = new ZipArchive();
        if (!$zip->open($zipPath, $flags)) {
            throw new FileSystemException(sprintf('cannot open zip file %s', $zipPath));
        }

        return $zip;
    }

    public function count(): int
    {
        return $this->getZipArchive(ZipArchive::RDONLY)->count();
    }

    /**
     * list files of a zip archive
     * @return array
     * @throws FileSystemException
     */
    public function listContent(): array
    {
        $files = [];

        $zip = $this->getZipArchive();

        for ($i = 0; $i < $zip->count(); $i++) {
            $files[] = $zip->getNameIndex($i);
        }
        $zip->close();

        return $files;
    }

    /**
     * @return bool
     */
    public function isDeleteSourcefiles(): bool
    {
        return $this->deleteSourcefiles;
    }

    /**
     * @param bool $deleteSourcefiles
     * @return Zip
     */
    public function setDeleteSourcefiles(bool $deleteSourcefiles): self
    {
        $this->deleteSourcefiles = $deleteSourcefiles;

        return $this;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }
}
