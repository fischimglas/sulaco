<?php
/**
 * Content marked with {{braces}} is going to be replaced with content from $data
 */
declare(strict_types=1);

namespace Sulaco\Portr;

use Sulaco\Contract\ReceiverInterface;
use Sulaco\Helper\StringHelper;
use Sulaco\Template\Template;

class TemplateDocument
{
    public string $contentRichText = '';
    public string $contentPlainText = '';
    public string $title = '';
    public string $subject = '';
    public string $style = '';
    public array $data = [];
    public array $attachments = [];
    public array $config = [];
    public array $receivers = [];
    public string $filename = '';
    public string $language = 'en';

    protected array $pageBorder = [
        'backtop' => '5mm',
        'backbottom' => '0',
        'backleft' => '10mm',
        'backright' => '25mm',
    ];
    protected string $pageOrientation = 'P';
    protected string $pageFormat = 'A4';
    protected string $defaultFont = 'Arial';

    public function getContentRichText(): string
    {
        return $this->compileTemplate($this->contentRichText);
    }

    public function setContentRichText(string $contentRichText): void
    {
        $this->contentRichText = $contentRichText;
    }

    public function compileTemplate(string $template): string
    {
        return (new Template($template, $this->getData()))->__toString();
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): void
    {
        $this->data = array_merge($this->data, $data);
    }

    public function getContentRichTextRaw(): string
    {
        return $this->contentRichText;
    }

    public function getContentPlainText(): string
    {
        return $this->compileTemplate($this->contentPlainText);
    }

    public function setContentPlainText(string $contentPlainText): void
    {
        $this->contentPlainText = $contentPlainText;
    }

    public function getDefaultFont(): string
    {
        return $this->defaultFont;
    }

    public function getPageBorder(): array
    {
        return $this->pageBorder;
    }

    public function getPageFormat(): string
    {
        return $this->pageFormat;
    }

    public function getPageOrientation(): string
    {
        return $this->pageOrientation;
    }

    public function getStyle(): string
    {
        return $this->style;
    }

    public function setStyle(string $style): void
    {
        $this->style = $style;
    }

    public function getSubject(): string
    {
        return $this->compileTemplate($this->subject);
    }

    public function setSubject(?string $subject = ''): void
    {
        $this->subject = $subject . '';
    }

    public function getTitle(): string
    {
        return $this->compileTemplate($this->title);
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getFilename(): string
    {
        return StringHelper::sanitizeFilename((new Template($this->filename, $this->getData()))->serialize());
    }

    public function setFilename(string $filename): void
    {
        $this->filename = $filename;
    }

    public function getAttachments(): array
    {
        return array_values($this->attachments);
    }

    public function setAttachments(array $attachments): void
    {
        foreach ($attachments as $attachment) {
            $this->addAttachment($attachment);
        }
    }

    public function addAttachment(array $attachment): void
    {
        $filename = $attachment['originalFilename'];
        $this->attachments[$filename] = $attachment;
    }

    public function getAttachment(string $originalFilename): ?array
    {
        return $this->attachments[$originalFilename] ?? null;
    }

    public function getReceivers(): array
    {
        return $this->receivers;
    }

    /**
     * @param ReceiverInterface[] $receivers
     */
    public function setReceivers(array $receivers): void
    {
        $this->receivers = $receivers;
    }

    public function addReceiver(ReceiverInterface $receiver): void
    {
        $this->receivers[] = $receiver;
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    public function setConfig(array $config): void
    {
        $this->config = $config;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function setLanguage(string $language): void
    {
        $this->language = $language;
    }

    public function setPageBorder(array $pageBorder): void
    {
        $this->pageBorder = $pageBorder;
    }

    public function setPageOrientation(string $pageOrientation): void
    {
        $this->pageOrientation = $pageOrientation;
    }

    public function setPageFormat(string $pageFormat): void
    {
        $this->pageFormat = $pageFormat;
    }

    public function setDefaultFont(string $defaultFont): void
    {
        $this->defaultFont = $defaultFont;
    }
}
