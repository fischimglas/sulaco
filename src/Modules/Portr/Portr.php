<?php
declare(strict_types=1);

namespace Sulaco\Portr;

use Sulaco\Contract\ChannelInterface;
use Sulaco\Portr\Channel\ProcessResult;

class Portr
{
    /**
     * @var ChannelInterface[]
     */
    private array $channels = [];
    private array $results = [];

    /**
     * @var TemplateDocument[]
     */
    private array $templateDocuments = [];

    /**
     * @param TemplateDocument|null $templateDocument
     */
    public function __construct(?TemplateDocument $templateDocument = null)
    {
        if ($templateDocument) {
            $this->addTemplateDocument($templateDocument);
        }
    }

    /**
     * @param TemplateDocument $templateDocument
     */
    public function addTemplateDocument(TemplateDocument $templateDocument): void
    {
        $this->templateDocuments[] = $templateDocument;
    }

    /**
     * @return ProcessResult[]
     */
    public function process(): array
    {
        foreach ($this->channels as $channel) {
            foreach ($this->templateDocuments as $document) {
                set_time_limit(3600);
                $this->results[] = $channel->processTemplate($document);
            }
        }

        return $this->getResults();
    }

    public function getResults(): array
    {
        return $this->results;
    }

    public function addChannel(ChannelInterface $channel): void
    {
        $this->channels[] = $channel;
    }

    public function getGeneratedFiles(): array
    {
        /** @var ProcessResult $result */
        return array_merge(...array_map(fn($it) => $it->getCreatedFiles(), $this->results));
    }
}
