<?php
/**
 * Channel Email - Sends Template to an E-Mail Receiver.
 */

declare(strict_types=1);

namespace Sulaco\Portr\Channel;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use PTag\Element;
use PTag\HtmlFactory;
use Sulaco\Contact\ReceiverRole;
use Sulaco\Contract\ChannelInterface;
use Sulaco\Contract\ReceiverInterface;
use Sulaco\Fs\File;
use Sulaco\Helper\HtmlHelper;
use Sulaco\Portr\TemplateDocument;
use Symfony\Component\CssSelector\Exception\ParseException;

class Email extends AbstractChannel implements ChannelInterface
{

    protected array $config = [
        'CharSet' => 'utf-8',
        'Encoding' => 'base64',
        'SMTPSecure' => true,
        'SMTPAuth' => true,
        'Username' => '',
        'Password' => '',
        'Priority' => '',
        'Host' => null,
        'Port' => null,
    ];

    protected ?string $basePath = null;

    /**
     * @throws Exception
     */
    public function processTemplate(TemplateDocument $templateDocument): ProcessResult
    {
        $this->setTemplateDocument($templateDocument);

        $mail = $this->configureEmail();

        $data = $templateDocument->getData();
        if (isset($data['lang'])) {
            $mail->setLanguage($data['lang']);
        }

        $mail->Subject = strip_tags($templateDocument->getSubject());
        $mail->AltBody = $templateDocument->getContentPlainText();
        $mail->Body = $this->prepareContentRichText();

        // NOTE: for unknown reason, getReceivers() only returns 1 entry.
        // DO NOT USE getReceivers!
        /** @var ReceiverInterface $rec */
        foreach ($templateDocument->receivers as $rec) {
            $email = $rec->getCoordinate()->email();
            $name = $rec->getCoordinate()->name();

            switch ($rec->getRole()) {
                case ReceiverRole::TO:
                    $mail->addAddress($email, $name);
                    break;
                case ReceiverRole::FROM:
                    $mail->setFrom($email, $name);
                    break;
                case ReceiverRole::CC:
                    $mail->addCC($email, $name);
                    break;
                case ReceiverRole::BCC:
                    $mail->addBCC($email, $name);
                    break;
                case ReceiverRole::REPLYTO:
                    $mail->addReplyTo($email, $name);
                    break;
            }
        }

        $attachments = $this->prepareAttachments();
        foreach ($attachments as $attachment) {
            // TODO!!
            if (isset($attachment['path'])) {
                $f = new File($attachment['path']);
            } else {
                $f = new File(realpath($this->basePath) . '/' . $attachment['filename']);
            }
            if ($f->fileExists()) {
                if ($attachment['type'] === 'inline') {
                    $mail->addEmbeddedImage(
                        $f->getAbsolutPath(),
                        $attachment['originalFilename'],
                        $attachment['originalFilename'],
                        PHPMailer::ENCODING_BASE64,
                        $attachment['contentType']
                    );
                } else {
                    $mail->AddAttachment(
                        $f->getAbsolutPath(),
                        $attachment['originalFilename'],
                        PHPMailer::ENCODING_BASE64,
                        $attachment['contentType']
                    );
                }
            }
        }

        $res = $mail->send();
        $p = new ProcessResult();
        $p->setSuccess($res);

        return $p;
    }

    /**
     * @throws Exception
     */
    protected function configureEmail(): PHPMailer
    {
        $cf = $this->config;
        $mail = new PHPMailer(true);
        $mail->isSMTP();
        $mail->isHtml();
        $mail->CharSet = $cf['CharSet'];
        $mail->Encoding = $cf['Encoding'];
        $mail->Host = $cf['Host'];
        $mail->Port = $cf['Port'];
        $mail->SMTPSecure = $cf['SMTPSecure'];
        $mail->SMTPAutoTLS = $cf['SMTPAutoTLS'];
        $mail->SMTPAuth = $cf['SMTPAuth'];
        $mail->Username = $cf['Username'];
        $mail->Password = $cf['Password'];
        $mail->Priority = $cf['Priority'];
        // Default config, Can be overwritten
        $mail->setFrom($cf['Sender']['email'], $cf['Sender']['name']);

        if (isset($cf['bccReceiver']) && is_array($cf['bccReceiver'])) {
            foreach ($cf['bccReceiver'] as $item) {
                $mail->addBCC($item['email'] ?? null, $item['name'] ?? null);
            }
        }

        return $mail;
    }

    /**
     * Prepare Content, ADD HTML HEADER, Inline CSS
     * @return string
     * @throws ParseException
     */
    public function prepareContentRichText(): string
    {
        $content = $this->getTemplateDocument()->getContentRichText();
        if ($content === '') {
            $content = $this->getTemplateDocument()->getContentPlainText();
        }

        $el = new Element();
        $el->add($this->getBodyHeader());
        $el->add(HtmlFactory::body(null, $content)->serialize());
        $el->add($this->getBodyFooter());

        return HtmlHelper::inlineCss($el->serialize(), $this->prepareStyle());
    }

    protected function getBodyHeader(): string
    {
        return '<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=' . $this->config['CharSet'] . '"></head>';
    }

    protected function getBodyFooter(): string
    {
        return '</html>';
    }

}
