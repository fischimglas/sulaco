<?php
declare(strict_types=1);

namespace Sulaco\Portr\Channel;

use Sulaco\Portr\TemplateDocument;

abstract class AbstractChannel
{

    protected array $config = [];

    protected TemplateDocument $templateDocument;

    public function __construct(array $config = [])
    {
        $this->setConfig($config);
    }

    public function setConfig(array $config): void
    {
        $this->config = array_merge($this->config, $config);
    }

    public function prepareAttachments(): array
    {
        return $this->getTemplateDocument()->getAttachments();
    }

    public function getTemplateDocument(): TemplateDocument
    {
        return $this->templateDocument;
    }

    public function setTemplateDocument(TemplateDocument $templateDocument): void
    {
        $this->templateDocument = $templateDocument;
    }

    public function prepareStyle(): string
    {
        return $this->getTemplateDocument()->getStyle();
    }

    public function prepareContentRichText(): string
    {
        return $this->getTemplateDocument()->getContentRichText();
    }
}
