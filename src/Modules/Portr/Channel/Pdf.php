<?php
/**
 * PDF Channel
 * TODO:
 * - configurable
 */
declare(strict_types=1);

namespace Sulaco\Portr\Channel;

use PTag\Element;
use PTag\HtmlFactory;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Html2Pdf;
use Sulaco\Contract\ChannelInterface;
use Sulaco\Fs\File;
use Sulaco\Portr\TemplateDocument;

class Pdf extends AbstractChannel implements ChannelInterface
{
    protected string $pageDelimiter = '<i data-pagebreak></i>';
    protected array $pageBorder = [
        'backtop' => '5mm',
        'backbottom' => '0',
        'backleft' => '10mm',
        'backright' => '25mm',
    ];
    protected string $pageOrientation = 'P';
    protected string $pageFormat = 'A4';
    protected string $defaultFont = 'Arial';

    protected ?string $basePath = null;

    public function getPageBorder(): array
    {
        return $this->pageBorder;
    }

    public function getDefaultFont(): string
    {
        return $this->defaultFont;
    }

    public function getPageFormat(): string
    {
        return $this->pageFormat;
    }

    public function getPageOrientation(): string
    {
        return $this->pageOrientation;
    }

    /**
     * @param TemplateDocument $templateDocument
     * @return ProcessResult
     * @throws Html2PdfException
     */
    public function processTemplate(TemplateDocument $templateDocument): ProcessResult
    {
        $this->setTemplateDocument($templateDocument);

        $this->setPageOrientation($templateDocument->getPageOrientation());
        $this->setDefaultFont($templateDocument->getDefaultFont());
        $this->setPageFormat($templateDocument->getPageFormat());
        $this->setPageBorder($templateDocument->getPageBorder());

        $filename = $templateDocument->getFilename();
        if (!$filename) {
            $filename = 'noname-' . time();
        }
        $filename .= '.pdf';

        $targetFile = new File('tmp/' . $filename);
        $targetFilePath = $targetFile->getAbsolutPath();

        // Render Pages
        $renderedPages = new Element();
        $content = $this->prepareContentRichText();
        $style = $this->prepareStyle();
        $pages = explode($this->pageDelimiter, $content);
        $renderedPages->add(new Element('style', null, $style));

        foreach ($pages as $page) {
            if (strlen($page) > 0) {
                $cnt = HtmlFactory::element('page', $this->pageBorder, $page)->serialize();
                $cnt = $this->replaceImagePaths($cnt);
                $renderedPages->add($cnt);
            }
        }

        $html2pdf = new Html2Pdf($this->pageOrientation, $this->pageFormat, 'en', true, 'UTF-8');

        $html2pdf->setDefaultFont($this->defaultFont);
        $html2pdf->writeHTML($renderedPages->serialize());
        $html2pdf->output($targetFilePath, 'F');

        $p = new ProcessResult();
        $p->setSuccess(true);
        $p->addCreatedFile($targetFilePath);

        return $p;
    }

    public function prepareContentRichText(): string
    {
        return $this->getTemplateDocument()->getContentRichText();
    }

    public function prepareStyle(): string
    {
        return $this->getTemplateDocument()->getStyle();
    }

    /**
     * @param string $cnt
     * @return string
     */
    public function replaceImagePaths(string $cnt): string
    {
        preg_match_all('/cid:([^"]+)/i', $cnt, $regs);
        foreach ($regs[1] as $i => $name) {
            $att = $this->findAttachmentByName($name);
            if ($att) {
                $f = new File(realpath($this->basePath) . '/' . $att['filename']);

                $path = $f->getAbsolutPath();
                if (!$f->fileExists()) {
                    $f = new File(__DIR__ . '/../../../../etc/null.png');
                    $path = $f->getAbsolutPath();
                }
                $cnt = preg_replace('/' . $regs[0][$i] . '/', $path, $cnt);
            }
        }

        return $cnt;
    }

    /**
     * @param string $name
     * @return array|null
     */
    private function findAttachmentByName(string $name): ?array
    {
        $files = $this->prepareAttachments();
        foreach ($files as $file) {
            if ($file['originalFilename'] === $name) {
                return $file;
            }
        }

        return null;
    }

    public function setDefaultFont(string $defaultFont): void
    {
        $this->defaultFont = $defaultFont;
    }

    public function setPageFormat(string $pageFormat): void
    {
        $this->pageFormat = $pageFormat;
    }

    public function setPageOrientation(string $pageOrientation): void
    {
        $this->pageOrientation = $pageOrientation;
    }

    public function setPageBorder(array $pageBorder): void
    {
        $this->pageBorder = $pageBorder;
    }
}
