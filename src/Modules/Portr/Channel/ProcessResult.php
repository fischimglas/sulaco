<?php
declare(strict_types=1);

namespace Sulaco\Portr\Channel;

class ProcessResult
{
    private ?bool $success = null;
    private array $createdFiles = [];

    public function addCreatedFile($file): void
    {
        $this->createdFiles[] = $file;
    }

    public function getCreatedFiles(): array
    {
        return $this->createdFiles;
    }

    public function setCreatedFiles(array $createdFiles): void
    {
        $this->createdFiles = $createdFiles;
    }

    public function addCreatedFiles(array $createdFile): void
    {
        $this->createdFiles[] = $createdFile;
    }

    public function getSuccess(): ?bool
    {
        return $this->success;
    }

    public function setSuccess(?bool $success): void
    {
        $this->success = $success;
    }
}
