<?php

declare(strict_types=1);

namespace Sulaco\Template;

use Mustache_Engine;
use PTag\SerializeableInterface;

class Template implements SerializeableInterface, TemplateInterface
{

    private string $template;
    private array $data;

    public function __construct(string $template = '', array $data = [])
    {
        $this->template = $template;
        $this->data = $data;
    }

    public function serialize(): string
    {
        return $this->compile();
    }

    public function compile(): string
    {
        $m = new Mustache_Engine;

        return $m->render($this->template, $this->data);
    }

    public function setData(array $data): void
    {
        $this->data = $data;
    }

    public function setTemplate(string $template): void
    {
        $this->template = $template;
    }

    public function __toString(): string
    {
        return $this->compile();
    }
}
