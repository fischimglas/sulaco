<?php
declare(strict_types=1);

namespace Sulaco\Helper;

use Spyc;
use Sulaco\Exception\FileSystemException;
use Sulaco\Fs\File;

class StringHelper
{
    /**
     * @param string $fileName
     * @return array
     * @throws FileSystemException
     */
    public static function parseYamlFile(string $fileName): array
    {
        $file = new File($fileName);

        return StringHelper::parseYaml($file->read());
    }

    /**
     * @param string $yamlString
     * @return array
     */
    public static function parseYaml(string $yamlString): array
    {
        return Spyc::YAMLLoad($yamlString);
    }

    /**
     * @param int $length
     * @return string
     */
    public static function generatePasswordHash(int $length = 8): string
    {
        $password_string = str_repeat('!@#$%*&abcdefghijklmnpqrstuwxyzABCDEFGHJKLMNPQRSTUWXYZ23456789', $length);

        return substr(str_shuffle($password_string), 0, $length);
    }

    /**
     * @param string $filename
     * @return string
     */
    public static function sanitizeFilename(string $filename): string
    {
        return preg_replace('/[^a-z0-9_-]+/i', '', strtr($filename, [' ' => '_',]));
    }

    /**
     * @param mixed $data
     * @param int|null $i
     * @return string
     */
    public static function flattenToHtml(mixed $data, ?int $i = 0): string
    {
        if (is_bool($data)) {
            return $data ? 'true' : 'false';
        } elseif (is_string($data) || is_numeric($data)) {
            return $data;
        } elseif (is_object($data)) {
            return print_r($data, true);
        } elseif (is_array($data)) {
            foreach ($data as $k => $v) {
                $tmp[] = str_repeat(' ', $i) . '- ' . $k . ':' . self::flattenToHtml($v, $i + 1);
            }

            return PHP_EOL . implode(PHP_EOL, $tmp);
        } else {
            return 'unknown type ' . gettype($data);
        }
    }
}
