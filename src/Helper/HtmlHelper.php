<?php

declare(strict_types=1);

namespace Sulaco\Helper;

use Pelago\Emogrifier\CssInliner;
use Symfony\Component\CssSelector\Exception\ParseException;

class HtmlHelper
{
    /**
     * @throws ParseException
     */
    public static function inlineCss(string $html, string $css): string
    {
        return CssInliner::fromHtml($html)->inlineCss($css)->render();
    }
}