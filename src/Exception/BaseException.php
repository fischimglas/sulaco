<?php

declare(strict_types=1);

namespace Sulaco\Exception;

use Exception;
use Sulaco\Contract\ExceptionInterface;
use Sulaco\Model\ExceptionSeverity;

class BaseException extends Exception implements ExceptionInterface
{
    protected string $severity = ExceptionSeverity::MEDIUM;

    public function getSeverity(): string
    {
        return $this->severity;
    }

    public function setSeverity(string $severity): void
    {
        $this->severity = $severity;
    }
}
