<?php

declare(strict_types=1);

namespace Sulaco\Core\Http\Response;

use JsonException;

class JsonResponse extends Response
{
    /**
     * @throws JsonException
     */
    public function __construct($json = null)
    {
        $this->setContentType(Response::CONTENT_TYPE_JSON);
        if(!is_null($json)) {
            $this->setContent(is_string($json) ? $json : json_encode($json, JSON_THROW_ON_ERROR));
        }
    }
}
