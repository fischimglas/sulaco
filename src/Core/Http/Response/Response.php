<?php

declare(strict_types=1);

namespace Sulaco\Core\Http\Response;


class Response implements ResponseInterface
{
    public const HTTP_OK = 200;
    public const HTTP_CREATED = 201;
    public const HTTP_ACCEPTED = 202;
    public const HTTP_NO_CONTENT = 204;

    public const HTTP_BAD_REQUEST = 400;
    public const HTTP_UNAUTHORIZED = 401;
    public const HTTP_PAYMENT_REQUIRED = 402;
    public const HTTP_FORBIDDEN = 403;
    public const HTTP_NOT_FOUND = 404;
    public const HTTP_METHOD_NOT_ALLOWED = 405;
    public const HTTP_NOT_ACCEPTABLE = 406;

    public const HTTP_SERVER_ERROR = 500;

    public const CONTENT_TYPE_HTML = 'text/html';
    public const CONTENT_TYPE_CSV = 'text/csv';
    public const CONTENT_TYPE_TSV = 'text/tsv';
    public const CONTENT_TYPE_CSS = 'text/css';
    public const CONTENT_TYPE_JS = 'application/javascript';
    public const CONTENT_TYPE_TEXT = 'text/plain';
    public const CONTENT_TYPE_JSON = 'application/json';
    public const CONTENT_TYPE_ZIP = 'application/zip';
    public const CONTENT_TYPE_OCTET_STREAM = 'application/octet-stream';

    public const CHARSET_UTF8 = 'utf-8';
    public const CHARSET_ISO8859 = 'ISO-8859-1';

    private bool $compress = true;
    private int $statusCode = self::HTTP_OK;

    private string $content = '';
    private string $contentType = self::CONTENT_TYPE_HTML;
    private ?string $charSet = self::CHARSET_UTF8;
    private array $headers = [
        'Cache-Control' => 'no-store, max-age=0',
        'Expires' => 0,
    ];

    public function addHeader(string $name, string $value): self
    {
        $this->headers[$name] = $value;

        return $this;
    }

    public function assembleHeaders(): array
    {
        $defaultHeaders = [
            'Content-Type' => $this->getContentType() . ($this->getCharSet() ? '; charset=' . $this->getCharSet() : ''),
        ];

        return array_merge($defaultHeaders, $this->getHeaders());
    }

    public function getCharSet(): ?string
    {
        return $this->charSet;
    }

    public function setCharSet(?string $charSet = null): self
    {
        $this->charSet = $charSet;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getContentType(): string
    {
        return $this->contentType;
    }

    public function setContentType(string $contentType): self
    {
        $this->contentType = $contentType;

        return $this;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function setStatusCode(int $statusCode): self
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function isCompressed(): bool
    {
        return $this->compress;
    }

    public function send(): void
    {
        header_remove('X-Powered-By');
        http_response_code($this->getStatusCode());
        $headers = $this->assembleHeaders();
        foreach ($headers as $k => $v) {
            header($k . ': ' . $v, true);
        }

        if ($this->compress) {
            ob_start('ob_gzhandler');
        } else {
            if (ob_get_length() > 0) {
                ob_end_clean();
            }
            flush();
        }

        echo $this->getContent();

        if ($this->compress && ob_get_length() > 0) {
            ob_end_flush();
        }
    }

    public function setCompress(bool $compress): self
    {
        $this->compress = $compress;

        return $this;
    }

}
