<?php

declare(strict_types=1);

namespace Sulaco\Core\Http\Response;


class TsvResponse extends Response
{
    public function __construct()
    {
        $this->setContentType(Response::CONTENT_TYPE_TSV);
    }
}