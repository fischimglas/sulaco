<?php

declare(strict_types=1);

namespace Sulaco\Core\Http\Response;

class FileResponse extends Response
{
    public const DISPOSITION_ATTACHMENT = 'attachment';
    public const DISPOSITION_INLINE = 'inline';
    private string $filePath;

    /**
     * @param string $filePath
     * @param string $disposition
     * @param string|null $filename
     */
    public function __construct(string $filePath, string $disposition = self::DISPOSITION_ATTACHMENT, ?string $filename = null)
    {
        if ($filename === null) {
            $filename = basename($filePath);
        }
        $this->setCompress(false);
        $this->filePath = $filePath;

        if (!file_exists($this->filePath)) {
            return false;
        }

        $this->setContentType(Response::CONTENT_TYPE_OCTET_STREAM);
        $this->setCharSet();
        $this->addHeader('Content-Transfer-Encoding', 'binary');
        $this->addHeader('Pragma', 'public');
        $this->addHeader('Content-Length', (string)filesize($this->filePath));
        $this->setCompress(false);

        if (strtolower($disposition) === self::DISPOSITION_ATTACHMENT) {
            $this->addHeader('Content-Disposition', self::DISPOSITION_ATTACHMENT . '; filename="' . $filename . '"');
        } else {
            $this->addHeader('Content-Disposition', self::DISPOSITION_INLINE);
        }
    }

    public function getContent(): string
    {
        return file_exists($this->filePath) ? file_get_contents($this->filePath) : '';
    }
}
