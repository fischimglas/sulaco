<?php

declare(strict_types=1);

namespace Sulaco\Core\Http\Response;

use JsonException;

class ResponseFactory
{
    /**
     * @throws JsonException
     */
    public static function json($json = null): JsonResponse
    {
        return new JsonResponse($json);
    }

    /**
     * @throws JsonException
     */
    public static function jsonSuccess(): JsonResponse
    {
        return new JsonResponse(['success' => true]);
    }

    public static function noContent(): Response
    {
        $r = new Response();
        $r->setStatusCode(Response::HTTP_NO_CONTENT);

        return $r;
    }

    public static function notFound(): Response
    {
        return new NotFoundResponse();
    }

    public static function file(string $filePath): FileResponse
    {
        return new FileResponse($filePath);
    }

    public static function html($content = null): Response
    {
        $r = new Response();
        $r->setContent($content);

        return $r;
    }

    public static function csv($content = null): CsvResponse
    {
        $r = new CsvResponse();
        $r->setContent($content);

        return $r;
    }


    public static function tsv($content = null): TsvResponse
    {
        $r = new TsvResponse();
        $r->setContent($content);

        return $r;
    }

    public static function text($content = null): Response
    {
        $r = new Response();
        $r->setContentType(Response::CONTENT_TYPE_TEXT);
        $r->setContent($content);

        return $r;
    }
}
