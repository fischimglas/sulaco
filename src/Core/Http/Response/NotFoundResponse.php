<?php
declare(strict_types=1);

namespace Sulaco\Core\Http\Response;

class NotFoundResponse extends Response
{
    public function __construct()
    {
        $this->setStatusCode(Response::HTTP_NOT_FOUND);
    }
}