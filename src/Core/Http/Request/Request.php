<?php

declare(strict_types=1);

namespace Sulaco\Core\Http\Request;

class Request
{
    /**
     * @return string|null
     */
    public static function parseAcceptedLanguageFromRequestHeader(): ?string
    {
        $headers = apache_request_headers();

        return $headers['Accept-Language'] ?? null;
    }

    /**
     * @return array|null
     */
    public static function readPayload(): ?array
    {
        return json_decode(file_get_contents('php://input'), true);
    }
}
