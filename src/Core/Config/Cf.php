<?php
declare(strict_types=1);

namespace Sulaco\Config;

use Sulaco\Exception\ConfigException;
use Sulaco\Exception\FileSystemException;
use Sulaco\Fs\File;
use Sulaco\Helper\StringHelper;

class Cf
{
    private static ?Cf $instance = null;

    private array $data = [];

    public static function set(string $name, $value = null): void
    {
        self::init()->_set($name, $value);
    }

    protected function _set(string $name, $value = null): void
    {
        $this->data[$name] = $value;
    }

    protected static function init(): Cf
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param string $yamlFile
     * @throws FileSystemException
     */
    public static function loadFromYaml(string $yamlFile): void
    {
        $f = new File($yamlFile);
        $yaml = StringHelper::parseYaml($f->read());
        foreach ($yaml as $key => $value) {
            self::init()->_set($key, $value);
        }
    }

    /**
     * @throws ConfigException
     */
    public static function get(?string $name = null)
    {
        return self::init()->_get($name);
    }

    /**
     * @throws ConfigException
     */
    protected function _get(?string $name = null)
    {
        if ($name && !$this->_isset($name)) {
            throw new ConfigException(sprintf("Config %s not set.", $name));
        }

        return $name ? $this->data[$name] : $this->data;
    }

    protected function _isset(string $name): bool
    {
        return isset($this->data[$name]);
    }

    public static function isset(string $name): bool
    {
        return self::init()->_isset($name);
    }
}
