<?php

declare(strict_types=1);

namespace Sulaco\Contact;

class ReceiverRole
{
  public const FROM = 'from';
  public const TO = 'to';
  public const CC = 'cc';
  public const BCC = 'bcc';
  public const REPLYTO = 'replyto';

  public const ROLES = [
    self::FROM,
    self::TO,
    self::CC,
    self::BCC,
    self::REPLYTO
  ];
}
