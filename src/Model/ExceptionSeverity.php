<?php
declare(strict_types=1);

namespace Sulaco\Model;

class ExceptionSeverity
{
    public const HIGH = 'high';
    public const  MEDIUM = 'medium';
    public const  LOW = 'low';
}