<?php

declare(strict_types=1);

namespace Sulaco\Contact;

class ContactType
{
  public const PRIVATE = 'private';
  public const COMPANY = 'company';
}
