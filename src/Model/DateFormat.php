<?php

declare(strict_types=1);

namespace Sulaco\Date;

class DateFormat
{
    public const FORMAT_DATE_H = 'd.m.y';
    public const FORMAT_DATE_HH = 'D. M Y';
    public const FORMAT_MYSQL_DATETIME = 'Y-m-d H:i:s';
    public const FORMAT_MYSQL_DATE = 'Y-m-d';
}
