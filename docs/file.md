# SULACO - FS
Handle files and directories 

### Usage
- read and write, move and delete files
- delete directories recursive

### Example
```php
use Sulaco\Fs\File;


File::setBasePath(__DIR__ . '/test/test2/test3');

$filePath = 'sample.txt';
$file = new File($filePath);

$result = [
    'write' => $file->write('Some Content'),
    'content' => $file->read(),
    'path' => $file->getAbsolutPath(),
    'fileExists' => $file->fileExists(),
    'move' => $file->moveTo('newFileName.txt'),
    'newpath' => $file->getAbsolutPath(),
    'basename' => $file->basename(),
    'getContentType' => $file->getContentType(),
];

print_r($result);

Array
(
    [write] => 1
    [content] => Some Content
    [path] => /test/test2/test3/sample.txt
    [fileExists] => 1
    [move] => 1
    [newpath] => /test/test2/test3/newFileName.txt
    [basename] => newFileName.txt
    [getContentType] => text/plain
)

```