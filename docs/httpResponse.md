# SULACO - HTTP Response
Create HTTP request response. Gzipped by default. 

### Usage
- Create any HTML Response with `ResponseFactory::<yourResponseType>()`
- Set attributes like:
  - `$response->setContentType(string)` 
  - `$response->setCharSet(string)` 
  - `$response->setStatusCode(string)` 
- Deploy response with `$response->send()` 

### Example

```php 
use Sulaco\Core\Http\Response\ResponseFactory;

$response = ResponseFactory::text('Some Text')
    ->setContentType(Response::CONTENT_TYPE_TEXT)
    ->setCharSet(Response::CHARSET_ISO8859)
    ->addHeader('X-manual-header','MyValue')
    ->setStatusCode(Response::HTTP_BAD_REQUEST)
    ->setCompress(false);

$response->send();
 ```

Available pre-confifgured responses:
- Html
- Text
- Json
- CSV
- TSV
- File
- 404 - Not found
- Json Success
- No Content
