# SULACO - HTTP Request
handle request data 

### Usage
- Read request data 
- Read request headers 

### Example

```php 
use Sulaco\Core\Http\Request\Request;

$data = Request::readPayload()
 ```

