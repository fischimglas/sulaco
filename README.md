
# SULACO

Simple PHP toolkit – jvo a.maurer – jvo.maurer@gmail.com

- [HTTP Response](docs/httpResponse.md) - Generate HTTP responses 
- [HTTP Request](docs/httpRequest.md) - Read request data
- [FileSystem](docs/httpRequest.md) - Read and modify files and directories
- [Zip](docs/httpRequest.md) - Manage ZIP files
